-- phpMyAdmin SQL Dump
-- version 4.2.5
-- http://www.phpmyadmin.net
--
-- Client :  localhost:8889
-- Généré le :  Mer 04 Novembre 2015 à 17:47
-- Version du serveur :  5.5.38
-- Version de PHP :  5.5.14

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Base de données :  `hangman_db`
--

-- --------------------------------------------------------

--
-- Structure de la table `dictionary`
--

CREATE TABLE `dictionary` (
`id` int(11) NOT NULL,
  `word` varchar(255) NOT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=21 ;

--
-- Contenu de la table `dictionary`
--

INSERT INTO `dictionary` (`id`, `word`) VALUES
(1, 'Hello'),
(2, 'good'),
(3, 'admit'),
(4, 'anonymous'),
(5, 'babysitting'),
(6, 'basically'),
(7, 'celebrating'),
(8, 'character'),
(9, 'details'),
(10, 'haters'),
(11, 'imagine'),
(12, 'incomes'),
(13, 'peruvian'),
(14, 'petrol'),
(15, 'quantity'),
(16, 'radiator'),
(17, 'salted'),
(18, 'seasons'),
(19, 'temperature'),
(20, 'ticket');

-- --------------------------------------------------------

--
-- Structure de la table `user`
--

CREATE TABLE `user` (
`id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `active` tinyint(1) NOT NULL,
  `nbgame` int(11) NOT NULL,
  `nbsuccess` int(11) NOT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=6 ;

--
-- Contenu de la table `user`
--

INSERT INTO `user` (`id`, `name`, `active`, `nbgame`, `nbsuccess`) VALUES
(1, 'Patrick', 0, 0, 0),
(3, 'Maxime', 0, 24, 13),
(4, 'Abdel', 1, 0, 0),
(5, 'Georges', 0, 1, 1);

--
-- Index pour les tables exportées
--

--
-- Index pour la table `dictionary`
--
ALTER TABLE `dictionary`
 ADD PRIMARY KEY (`id`);

--
-- Index pour la table `user`
--
ALTER TABLE `user`
 ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT pour les tables exportées
--

--
-- AUTO_INCREMENT pour la table `dictionary`
--
ALTER TABLE `dictionary`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=21;
--
-- AUTO_INCREMENT pour la table `user`
--
ALTER TABLE `user`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=6;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
