package com.mendewelt;

import java.util.ArrayList;
import java.util.List;

public class Game {
	private Word word;
	private List<String> foundletters = new ArrayList<String>();
	private int nbTry;
	
	public Word getWord() {
		return word;
	}
	public void setWord(Word word) {
		this.word = word;
	}
	public List<String> getFoundletters() {
		return foundletters;
	}
	public void setFoundletters(List<String> foundletters) {
		this.foundletters = foundletters;
	}
	public int getNbTry() {
		return nbTry;
	}
	public void setNbTry(int nbTry) {	
		this.nbTry = nbTry;
	}
	
	
}
