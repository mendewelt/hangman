package com.mendewelt;


import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Random;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.mendewelt.dao.UserDAO;
import com.mendewelt.dao.WordDAO;

public class MyServlet extends HttpServlet{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private final Map<HttpSession, User> clients = new HashMap<HttpSession, User>();
	private final Map<User, Game> games = new HashMap<User, Game>();
	private static final int NBWORD = 20;

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		
		HttpSession session = req.getSession();
		User user = null;
		Game game = null;
		
		
		
		
		String option = req.getParameter("option");
		
		/*The user refresh the page*/
		if(option == null){
			
			RequestDispatcher requestDispatcher = req.getRequestDispatcher("/index.jsp") ;
		    requestDispatcher.forward(req, resp);
		    return;
		}
		
		
		switch (option) {
		case "newword":
			
			
			WordDAO wordDao = new WordDAO();
			Word word = null;
			Random rand = new Random();
			int index = rand.nextInt(NBWORD) + 1;
			try {
				word = wordDao.getWord(index);
			} catch (SQLException e) {
				e.printStackTrace();
			}
			
			System.out.println("Get Game CALLED");
			System.out.println(word.getWord());
			
			user = clients.get(session);
			
			game = new Game();
			game.setWord(word);
			games.put(user, game);

			resp.setContentType("text/plain");
			resp.getWriter().write(word.getWord());
			break;

		case "refresh":
			System.out.println("Get Refresh CALLED");
			user = clients.get(session);
			game = games.get(user);
			
			if(game != null){
				System.out.println("Game find");
				ObjectMapper mapper = new ObjectMapper();
				resp.setContentType("application/json");
				mapper.writeValue(System.out, game);
				mapper.writeValue(resp.getOutputStream(), game);
			}
			
			break;
			
		case "getstat":
			System.out.println("Get Stat CALLED");
			
			List<User> users = new ArrayList<User>();
			Iterator it = clients.entrySet().iterator();
			System.out.println(clients.size());
		    while (it.hasNext()) {
		        Map.Entry pair = (Map.Entry)it.next();
		        System.out.println(pair.getValue());
		        users.add((User) pair.getValue());
		    }
			
		    session.setAttribute("clients", clients);
			
			RequestDispatcher requestDispatcher = req.getRequestDispatcher("/historic.jsp") ;
		    requestDispatcher.forward(req, resp);
			
		   
			
		default:
			break;
		}

		
	}
	
	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		
		HttpSession session = req.getSession();
		String username = req.getParameter("username");
		
		UserDAO userDao = new UserDAO();
		
		
		String option = req.getParameter("option");
		switch (option) {
		case "loguser": 
			System.out.println("POST CALLED : Have to check if the user exists");
			
			User user = null;
			try {
				user = userDao.getUser(username);
				if(user == null){
					System.out.println("CREATION OF NEW USER");
					user = new User();
					user.setName(username);
					userDao.insert(user);
				}
				if(!clients.containsKey(session)){
					clients.put(session, user);
				}
					
			} catch (SQLException e) {
				e.printStackTrace();
			}
			session.setAttribute("username", username);
			
		    RequestDispatcher requestDispatcher = req.getRequestDispatcher("/index.jsp") ;
		    requestDispatcher.forward(req, resp);
			break;
		
		case "endgame" : 
			System.out.println("End Game");
			String success = req.getParameter("success");
			user = clients.get(req.getSession());
			user.setNbGame(user.getNbGame()+1);
			if(success.equals("yes")){
				user.setNbSuccess(user.getNbSuccess()+1);
			}
			break;
			
		case "update" :
			System.out.println("Update Game status");
			String foundLetters = req.getParameter("letters");
			char[] let = foundLetters.toCharArray();
			
			int nbTry = Integer.valueOf(req.getParameter("nbtry"));
			ArrayList<String> list = new ArrayList<String>();
			
			user = clients.get(session);
			Game game = games.get(user);
			
			for(int i = 0; i< let.length; i++){
				list.add(Character.toString(let[i]));
			}
			
			game.setNbTry(nbTry);
			game.setFoundletters(list);
			
			break;
			
		case "endsession":
			System.out.println("End Session");
			user = clients.get(req.getSession());
			userDao.update(user);
		
			//Remove the user from the online users collection
			Iterator it = clients.entrySet().iterator();
			System.out.println(clients.size());
		    while (it.hasNext()) {
		        Map.Entry pair = (Map.Entry)it.next();
		        User u = (User) pair.getValue();
		        if(user.equals(u)){
		        	it.remove();
		        }
		    }
			session.invalidate();	
			resp.sendRedirect("http://localhost:8080/Hangman/home.jsp");
			break;
			
		default:System.err.println("Should never happened");
		}
		
		

		
		
		

		
		
		
	}
	
}
