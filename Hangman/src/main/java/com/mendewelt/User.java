package com.mendewelt;

public class User {
	private int id;
	private String name;
	private boolean active;
	private int nbGame;
	private int nbSuccess;
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public boolean isActive() {
		return active;
	}
	public void setActive(boolean active) {
		this.active = active;
	}
	public int getNbGame() {
		return nbGame;
	}
	public void setNbGame(int nbGame) {
		this.nbGame = nbGame;
	}
	public int getNbSuccess() {
		return nbSuccess;
	}
	public void setNbSuccess(int nbSuccess) {
		this.nbSuccess = nbSuccess;
	}
	
	@Override
	public boolean equals(Object obj) {
		User user = (User) obj;
		return this.getId() == user.getId();
	}
	
	
}
