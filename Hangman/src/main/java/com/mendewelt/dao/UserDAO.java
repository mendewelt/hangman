package com.mendewelt.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import com.mendewelt.User;
import com.mendewelt.db.ConnectionFactory;
import com.mendewelt.db.DBUtil;


public class UserDAO {
	private Connection connection;
	private Statement statement;

	public UserDAO() { }

	public User getUser(String username) throws SQLException {
		String query = "SELECT * FROM user WHERE name= " + "'"+username+"'";
		ResultSet rs = null;
		User user = null;
		try {
			connection = ConnectionFactory.getConnection();
			statement = connection.createStatement();
			rs = statement.executeQuery(query);
			if (rs.next()) {
				user = new User();
				user.setId(rs.getInt("id"));
				user.setName(rs.getString("name"));
				user.setActive(rs.getBoolean("active"));
				user.setNbGame(rs.getInt("nbgame"));
				user.setNbSuccess(rs.getInt("nbsuccess"));
			}

		} finally {
			DBUtil.close(rs);
			DBUtil.close(statement);
			DBUtil.close(connection);
		}
		return user;
	}


	public void insert(User user){
		String query = "INSERT INTO user(name,active,nbgame,nbsuccess)"
				+ "VALUES (?,?,?,?)";

		try {
			connection = ConnectionFactory.getConnection();
			PreparedStatement st = connection.prepareStatement(query);

			st.setString(1, user.getName());
			st.setBoolean(2, true);
			st.setInt(3, 0);
			st.setInt(4, 0);

			st.execute();

		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			DBUtil.close(statement);
			DBUtil.close(connection);
		}
	}

	public void update(User user){

		String query = "UPDATE user SET name=?, active=?, nbgame=?, nbsuccess=? WHERE id=?";

		try {
			connection = ConnectionFactory.getConnection();
			PreparedStatement st = connection.prepareStatement(query);

			st.setString(1, user.getName());
			st.setBoolean(2, false);
			st.setInt(3, user.getNbGame());
			st.setInt(4, user.getNbSuccess());
			st.setInt(5, user.getId());

			st.execute();

		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			DBUtil.close(statement);
			DBUtil.close(connection);
		}
	}
}
