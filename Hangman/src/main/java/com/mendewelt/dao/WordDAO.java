package com.mendewelt.dao;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import com.mendewelt.Word;
import com.mendewelt.db.ConnectionFactory;
import com.mendewelt.db.DBUtil;

public class WordDAO {
	private Connection connection;
	private Statement statement;

	public WordDAO() { }

	public Word getWord(int wordid) throws SQLException {
		String query = "SELECT * FROM dictionary WHERE id=" + wordid;
		ResultSet rs = null;
		Word word = null;
		try {
			connection = ConnectionFactory.getConnection();
			statement = connection.createStatement();
			rs = statement.executeQuery(query);
			if (rs.next()) {
                word = new Word();
                word.setId(rs.getInt("id"));
                word.setWord(rs.getString("word"));
            }
			
		} finally {
			DBUtil.close(rs);
			DBUtil.close(statement);
			DBUtil.close(connection);
		}
		return word;
	}
}
