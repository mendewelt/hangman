<%@page import="com.mendewelt.User"%>
<%@page import="java.util.Iterator"%>
<%@page import="java.util.Map"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link rel='stylesheet'
	href='webjars/bootstrap/3.2.0/css/bootstrap.min.css'>
<link type="text/css" rel="stylesheet" href="mystyle.css" />
<title>Current players</title>
</head>
<body>

<h1>Online players</h1>	
<div class="container">
	<% Map clients = (Map)request.getSession().getAttribute("clients"); %>

	<div class="row">
		<div class="table-responsive">
      <table class="table table-striped table-condensed">
        <thead>
          <tr>
            <th>#</th>
            <th>Name</th>
            <th>Party played</th>
            <th>Success</th>
          </tr>
        </thead>
        <tbody>
          <%
          int i = 1;
          Iterator it = clients.entrySet().iterator();
		  System.out.println(clients.size());
		  while (it.hasNext()) {
			 Map.Entry pair = (Map.Entry)it.next();
		     User user = (User) pair.getValue();
		  %>     
		  	  <tr>
		  		<td><%= i %></td>
		  		<td><%= user.getName()%></td>
		  		<td><%= user.getNbGame()%></td>
		  		<td><%= user.getNbSuccess()%></td>
		  	 </tr>	
		  		<%i++;} %>
		  
        </tbody>
      </table>
    </div>
	</div>
</div>

</body>
</html>