<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link rel='stylesheet'
	href='webjars/bootstrap/3.2.0/css/bootstrap.min.css'>
<link type="text/css" rel="stylesheet" href="mystyle.css" />
<script
	src="http://ajax.googleapis.com/ajax/libs/jquery/1.7.2/jquery.js"
	type="text/javascript"></script>

<script
	src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.8/jquery-ui.min.js"
	type="text/javascript"></script>
	
<script type="text/javascript">

$(document)
.ready(
		function() {
			
			document.getElementById("log").disabled = true;
			
			$("#firstname").keyup(function(){
				document.getElementById("log").disabled = false;
			})
		});
		
		
</script>	
<title>Home</title>
</head>
<body>
	<header class="myheader hero-spacer">
	<h1>Welcome to HangMan</h1>
	<p>You will loose your head</p>

	<form method="POST" action="http://localhost:8080/Hangman/play">
		<div class="form-group col-lg-2">
			<label for="exampleInputPassword1">First name</label> <input
				type="text" class="form-control" id="firstname" name="username"
				placeholder="Firstname">
				<input type="hidden" name="option" value="loguser"/>
		</div>
		<button type="submit" id="log" class="btn btn-primary">Play</button>
	</form>

	</header>

</body>
</html>