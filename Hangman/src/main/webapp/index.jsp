<%@page import="java.util.Map"%>
<html>
<head>
<meta name="viewport" content="initial-scale=1, maximum-scale=1">
<link rel='stylesheet'
	href='webjars/bootstrap/3.2.0/css/bootstrap.min.css'>
<link type="text/css" rel="stylesheet" href="mystyle.css" />
<script
	src="http://ajax.googleapis.com/ajax/libs/jquery/1.7.2/jquery.js"
	type="text/javascript"></script>

<script
	src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.8/jquery-ui.min.js"
	type="text/javascript"></script>

<script type="text/javascript">
	$(document)
			.ready(
					function() {
						word = new Array();
						founds = new Array();
						var nbTrial = 5;
						var trial;
						var foundletter;
						var secretWord;

						$('#replay').hide();
						$('#myText').hide();
						$('#mySecretWord').hide();
						$('.alert').hide();
						
						var game = new Object();
						
						$.get("play", {
							option : 'refresh'
							
						}, function(data) {
							var wordPlayed;
							
									wordPlayed = data.word.word;
									trial = data.nbTry;
									$.each(data.foundletters,function(i,letter){
										founds[i] = letter;
									})
								
								initGame();
								launch();
								foundletter = 0;
								$("#mySecretWord").text(wordPlayed);
								secretWord = $("#mySecretWord")
								.text();
								
								for (var i = 0; i < wordPlayed.length; i++) {
									var text = $(
											"#mot tr")
											.html();
									$("#mot tr")
											.html(
													text
															+ "<td> <p id=\""+i+"\">"
															+ wordPlayed.charAt(i)
															+ "</p></td>");
									word[i] = document
											.getElementById(i);
									
									for(var j = 0; j< founds.length; j++){
										
										if (secretWord.charAt(i) == founds[j]) {
											document
											.getElementById(i).style.visibility = 'visible';
											foundletter++;
										}
									}
									
								}
								
									
								
								
								
							
							
							
							
						});

						$('.play')
								.click(
										function() {

											foundletter = 0;
											trial = 0
											launch();

											$
													.get(
															"play",
															{
																option : 'newword'
															},
															function(data) {
																for (var i = 0; i < data.length; i++) {
																	var text = $(
																			"#mot tr")
																			.html();
																	$("#mot tr")
																			.html(
																					text
																							+ "<td> <p id=\""+i+"\">"
																							+ data
																									.charAt(i)
																							+ "</p></td>");
																	word[i] = document
																			.getElementById(i);
																}
																$(
																		"#mySecretWord")
																		.text(
																				data);
															});

										});

						$('.stat').click(function() {
							$.get("play", {
								option : 'getstat'
							}, function(data) {

							});

						});

						$('.try')
								.click(
										function() {
											secretWord = $("#mySecretWord")
													.text();
											var submitValue = $("#letterSubmit")
													.val();

											var find = false;

											for (var i = 0; i < secretWord.length; i++) {
												if (secretWord.charAt(i) == submitValue) {
													document.getElementById(i).style.visibility = 'visible';
													find = true;
													founds[foundletter] = submitValue;
													foundletter++;
												}
											}

											
											if (!find) {
												trial++;
												$('.alert')
														.text(
																nbTrial
																		- trial
																		+ " try remaining");

												if (trial == nbTrial) {
													for (var i = 0; i < secretWord.length; i++)
														document
																.getElementById(i).style.visibility = 'visible';
													alert("Game Over!");
													$.post("play", {
														option : 'endgame',
														success : 'no'
													}, function(data) {

													});
													$("#letterSubmit").hide();
													$('.try').hide();
													$("#tapealetter").hide();
													initGame();
												} else if (nbTrial - trial == 3) {
													$('.alert')
															.removeClass(
																	"alert-success")
															.addClass(
																	"alert-warning");
												} else if (nbTrial - trial == 1) {
													$('.alert')
															.removeClass(
																	"alert-warning")
															.addClass(
																	"alert-danger");
												}
											}

											
											
											if (foundletter == secretWord.length) {
												initGame();
												alert("Congratulation !");
												$.post("play", {
													option : 'endgame',
													success : 'yes'
												}, function(data) {

												});
												$("#letterSubmit").hide();
												$('.try').hide();
												$("#tapealetter").hide();

											}

											$("#letterSubmit").val('');
											updateGameStatus();
										});
						
						function updateGameStatus(){
							var foundLetters = "";
							//alert(founds.length+" "+founds);
							for (var i = 0; i < founds.length; i++) {
								foundLetters += founds[i];
								//alert("add a letter :"+ founds[i]);
							} 
							
							//alert("Letter found to send : "+foundLetters);
							
							
							$.post("play", {
								option : 'update',
								nbtry : trial,
								letters : foundLetters
							}, function(data) {
	
							});
						}

						function initGame() {
							$('#replay').show();
							$('#letters').each(function() {
								$(this).children('td').remove();
							})
							$('.alert').hide();
						}
						
						function launch(){
							$('#replay').hide();
							$('.play').hide();
							$('#myText').show();
							$('.alert').show();
							$('.alert')
									.removeClass(
											"alert-warning")
									.removeClass("alert-danger")
									.addClass("alert-success")
									.text(
											nbTrial - trial
													+ " try remaining");
							$('#letterSubmit').show();
							$('.try').show();
							$("#tapealetter").show();

						}

					});
</script>

</head>


<body>

	<%
		String username = (String) request.getSession().getAttribute(
				"username");
	%>
	<h1>
		Welcome
		<%=username%></h1>

	<div class="well col-lg-5">
		This is a Hangman game developed by Maxime Endewelt<br>Please
		submit one minimal letter at a time, Press "Play!" to launch the game
	</div>
	<div class="col-lg-8">
		<a href="#" class="play btn btn-primary btn-large">Play!</a>
	</div>


	<div class="col-lg-8" id="myText">
		<p id="mySecretWord"></p>

		<span id="tapealetter">Tape a letter:</span> <input id="letterSubmit"
			type="text" maxlength=1> <a href="#"
			class="try btn btn-primary btn-large">Submit!</a> <a href="#"
			id="replay" class="play btn btn-primary btn-large">New game!</a>

	</div>

	<div class="col-lg-8">
		<table id="mot">
			<tr id="letters"></tr>
		</table>
	</div>

	<div class="col-lg-8">
		<div class="alert alert-success col-lg-3" role="alert">5 try
			remaining</div>
	</div>
	<div class="col-lg-8">
		<form method="GET" action="http://localhost:8080/Hangman/play">
			<div class="form-group col-lg-2">
				<input type="hidden" name="option" value="getstat" />
			</div>
			<button type="submit" class="btn btn-primary">Get Current
				play!</button>
		</form>

		<form method="POST" action="http://localhost:8080/Hangman/play">
			<div class="form-group col-lg-2">
				<input type="hidden" name="option" value="endsession" />
			</div>
			<button type="submit" class="btn btn-warning">Log out</button>
		</form>
	</div>





</body>
</html>
